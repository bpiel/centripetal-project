(ns com.billpiel.indicator.core)

(defn indicator [auth-user slug])

(defn create-indicator! [auth-user indicator-input])

(defn update-indicator! [auth-user slug indicator-input])

(defn delete-indicator! [auth-user slug])

(defn indicators [auth-user limit offset author tag favorited])

