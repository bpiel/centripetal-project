(ns com.billpiel.indicator.interface
    (:require [com.billpiel.indicator.core :as core]))

(defn indicator [auth-user slug]
  (core/indicator auth-user slug))

(defn create-indicator! [auth-user indicator-input]
  (core/create-indicator! auth-user indicator-input))

(defn update-indicator! [auth-user slug indicator-input]
  (core/update-indicator! auth-user slug indicator-input))

(defn delete-indicator! [auth-user slug]
  (core/delete-indicator! auth-user slug))

(defn indicators [auth-user limit offset author tag favorited]
  (core/indicators auth-user limit offset author tag favorited))
