(ns com.billpiel.indicator.store
  (:require [clojure.java.jdbc :as jdbc]
            [com.billpiel.database.interface :as database]
            [clojure.string :as str]
            [honeysql.core :as sql]))

(comment
  (defn find-by-slug [slug]
    (let [query {:select [:*]
                 :from   [:indicator]
                 :where  [:= :slug slug]}
          results (jdbc/query (database/db) (sql/format query) {:identifiers identity})]
      (first results)))

  (defn insert-indicator! [indicator-input]
    (jdbc/insert! (database/db) :indicator indicator-input {:entities identity}))

  (defn tags-with-names [tag-names]
    (let [query {:select [:*]
                 :from   [:tag]
                 :where  [:in :name tag-names]}
          results (jdbc/query (database/db) (sql/format query))]
      results))

  (defn add-tags-to-indicator! [indicator-id tag-names]
    (when-not (empty? tag-names)
      (let [tags (tags-with-names tag-names)
            inputs (mapv #(hash-map :indicatorId indicator-id
                                    :tagId (:id %))
                         tags)]
        (jdbc/insert-multi! (database/db) :indicatorTags inputs))))

  (defn indicator-tags [indicator-id]
    (let [query {:select [:name]
                 :from   [:indicatorTags]
                 :join   [:tag [:= :tagId :tag.id]]
                 :where  [:= :indicatorId indicator-id]}
          results (jdbc/query (database/db) (sql/format query))]
      (mapv :name results)))

  (defn insert-tag! [name]
    (let [tag (first (tags-with-names [name]))]
      (when-not tag
        (jdbc/insert! (database/db) :tag {:name name}))))

  (defn update-tags! [tag-names]
    (when-not (empty? tag-names)
      (doseq [name tag-names]
        (insert-tag! name))))

  (defn favorited? [user-id indicator-id]
    (let [query {:select [:%count.*]
                 :from   [:favoriteIndicators]
                 :where  [:and [:= :indicatorId indicator-id]
                          [:= :userId user-id]]}
          result (jdbc/query (database/db) (sql/format query))]
      (< 0 (-> result first first val))))

  (defn favorites-count [indicator-id]
    (let [query {:select [:%count.*]
                 :from   [:favoriteIndicators]
                 :where  [:= :indicatorId indicator-id]}
          result (jdbc/query (database/db) (sql/format query))]
      (-> result first first val)))

  (defn update-indicator! [id indicator-input]
    (let [query {:update :indicator
                 :set    indicator-input
                 :where  [:= :id id]}]
      (jdbc/execute! (database/db) (sql/format query))))

  (defn delete-indicator! [id]
    (let [query-1 {:delete-from :indicatorTags
                   :where       [:= :indicatorId id]}
          query-2 {:delete-from :favoriteIndicators
                   :where       [:= :indicatorId id]}
          query-3 {:delete-from :indicator
                   :where       [:= :id id]}]
      (jdbc/with-db-transaction [trans-conn (database/db)]
        (jdbc/execute! trans-conn (sql/format query-1))
        (jdbc/execute! trans-conn (sql/format query-2))
        (jdbc/execute! trans-conn (sql/format query-3)))
      nil))

  (defn favorite! [user-id indicator-id]
    (when-not (favorited? user-id indicator-id)
      (jdbc/insert! (database/db) :favoriteIndicators {:indicatorId indicator-id
                                                       :userId    user-id})))

  (defn unfavorite! [user-id indicator-id]
    (when (favorited? user-id indicator-id)
      (let [query {:delete-from :favoriteIndicators
                   :where       [:and [:= :indicatorId indicator-id]
                                 [:= :userId user-id]]}]
        (jdbc/execute! (database/db) (sql/format query)))))

  (defn feed [user-id limit offset]
    (let [query {:select   [:a.*]
                 :from     [[:indicator :a]]
                 :join     [[:userFollows :u] [:= :a.userId :u.followedUserId]]
                 :where    [:= :u.userId user-id]
                 :order-by [[:a.updatedAt :desc] [:a.id :desc]]
                 :limit    limit
                 :offset   offset}
          results (jdbc/query (database/db) (sql/format query) {:identifiers identity})]
      results))

  (defn indicators-by-tag [limit offset tag]
    (let [query {:select   [:a.*]
                 :from     [[:indicator :a]]
                 :join     [[:indicatorTags :at] [:= :a.id :at.indicatorId]
                            [:tag :t] [:= :at.tagId :t.id]]
                 :where    [:= :t.name tag]
                 :order-by [[:updatedAt :desc] [:id :desc]]
                 :limit    limit
                 :offset   offset}
          results (jdbc/query (database/db) (sql/format query) {:identifiers identity})]
      results))

  (defn indicators-by-author [limit offset author]
    (let [query {:select   [:a.*]
                 :from     [[:indicator :a]]
                 :join     [[:user :u] [:= :a.userId :u.id]]
                 :where    [:= :u.username author]
                 :order-by [[:updatedAt :desc] [:id :desc]]
                 :limit    limit
                 :offset   offset}
          results (jdbc/query (database/db) (sql/format query) {:identifiers identity})]
      results))

  (defn indicators-by-favorited [limit offset favorited]
    (let [query {:select   [:a.*]
                 :from     [[:indicator :a]]
                 :join     [[:favoriteIndicators :fa] [:= :a.id :fa.indicatorId]
                            [:user :u] [:= :fa.userId :u.id]]
                 :where    [:= :u.username favorited]
                 :order-by [[:updatedAt :desc] [:id :desc]]
                 :limit    limit
                 :offset   offset}
          results (jdbc/query (database/db) (sql/format query) {:identifiers identity})]
      results))

  (defn all-indicators [limit offset]
    (let [query {:select   [:*]
                 :from     [:indicator]
                 :order-by [[:updatedAt :desc] [:id :desc]]
                 :limit    limit
                 :offset   offset}
          results (jdbc/query (database/db) (sql/format query) {:identifiers identity})]
      results))

  (defn indicators [limit offset author tag favorited]
    (if-not (str/blank? author)
      (indicators-by-author limit offset author)
      (if-not (str/blank? tag)
        (indicators-by-tag limit offset tag)
        (if-not (str/blank? favorited)
          (indicators-by-favorited limit offset favorited)
          (all-indicators limit offset)))))
  )
