(ns com.billpiel.database.core
  (:require [clojure.java.io :as io]
            [com.billpiel.env.interface :as env]))

(comment
  (defn- db-path []
    (if (contains? env/env :database)
      (env/env :database)
      "database.db"))

  (defn db
    ([path]
     {:classname   "org.sqlite.JDBC"
      :subprotocol "sqlite"
      :subname     path})
    ([]
     (db (db-path))))

  (defn db-exists? []
    (let [db-file (io/file "database.db")]
      (.exists db-file))))
