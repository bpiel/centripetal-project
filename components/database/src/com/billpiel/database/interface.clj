(ns com.billpiel.database.interface
  (:require [com.billpiel.database.core :as core]
        #_    [com.billpiel.database.schema :as schema]))

(comment
  (defn db
    ([path]
     (core/db path))
    ([]
     (core/db)))

  (defn db-exists? []
    (core/db-exists?))

  (defn generate-db [db]
    (schema/generate-db db))

  (defn drop-db [db]
    (schema/drop-db db))

  (defn valid-schema? [db]
    (schema/valid-schema? db)))
