(ns com.billpiel.log.interface
  (:require [com.billpiel.log.core :as core]))

(defmacro info [& args]
  `(core/info ~args))

(defmacro warn [& args]
  `(core/info ~args))

(defmacro error [& args]
  `(core/error ~args))
