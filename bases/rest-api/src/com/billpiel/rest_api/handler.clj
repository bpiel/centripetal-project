(ns com.billpiel.rest-api.handler
  (:require [clojure.edn :as edn]
            [com.billpiel.indicator.interface :as indicator]
            #_[com.billpiel.indicator.interface.spec :as indicator-spec]
            [clojure.spec.alpha :as s]
            [com.billpiel.env.interface :as env]))


(defn indicators [req]
  "hello!"
  #_(let [auth-user (-> req :auth-user)
        limit (parse-query-param (-> req :params :limit))
        offset (parse-query-param (-> req :params :offset))
        author (-> req :params :author)
        tag (-> req :params :tag)
        favorited (-> req :params :favorited)
        [ok? res] (indicator/indicators auth-user
                                    (if (pos-int? limit) limit nil)
                                    (if (nat-int? offset) offset nil)
                                    (if (string? author) author nil)
                                    (if (string? tag) tag nil)
                                    (if (string? favorited) favorited nil))]
    (handle (if ok? 200 404) res)))

(defn indicator [req]
  #_(let [auth-user (-> req :auth-user)
        slug (-> req :params :slug)]
    (if (s/valid? spec/slug? slug)
      (let [[ok? res] (indicator/indicator auth-user slug)]
        (handle (if ok? 200 404) res))
      (handle 422 {:errors {:slug ["Invalid slug."]}}))))
