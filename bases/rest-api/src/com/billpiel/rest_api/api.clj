(ns com.billpiel.rest-api.api
  (:require #_[com.billpiel.database.interface :as database]
            [com.billpiel.rest-api.handler :as h]
            #_[com.billpiel.rest-api.middleware :as m]
            #_[com.billpiel.log.interface :as log]
            [compojure.core :refer [routes wrap-routes defroutes GET POST PUT DELETE ANY OPTIONS]]
            [ring.logger.timbre :as logger]
            [ring.middleware.json :as js]
            [ring.middleware.keyword-params :as kp]
            [ring.middleware.multipart-params :as mp]
            [ring.middleware.nested-params :as np]
            [ring.middleware.params :as pr]))

(defroutes public-routes
#_  (OPTIONS "/**"                              [] h/options)
  (GET     "/indicators"                    [] h/indicators)
  (GET     "/indicators/:slug"              [] h/indicator))

(def app-routes
  (routes
    public-routes))

;; TODO !!! review all middleware
(def app
  (-> app-routes
      ;; logger/wrap-with-logger
      ;; kp/wrap-keyword-params
      ;; pr/wrap-params
      ;; mp/wrap-multipart-params
      ;; js/wrap-json-params
      ;; np/wrap-nested-params
      ;; m/wrap-exceptions
      ;; js/wrap-json-response
      ;; m/wrap-cors
      ))

(defn init []
#_  (try
    (log/init)
    (let [db (database/db)]
      (if (database/db-exists?)
        (if (database/valid-schema? db)
          (log/info "Database schema is valid.")
          (do
            (log/warn "Please fix database schema and restart")
            (System/exit 1)))
        (do
          (log/info "Generating database.")
          (database/generate-db db)
          (log/info "Database generated."))))
    (log/info "Initialized server.")
    (catch Exception e
      (log/error e "Could not start server."))))

(defn destroy []
#_  (log/info "Destroyed server."))
